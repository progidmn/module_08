variable "username" {
    description = "username from vkcs"
    type = string
    sensitive = true
}
variable "password" {
    description = "password from vkcs"
    type = string
    sensitive = true
}
variable "project_id" {
    description = "project_id from vkcs"
    type = string
    sensitive = true
}