resource "vkcs_compute_keypair" "keypair" {
  name = "test-keypair"
}

output "public_key" {
  value = vkcs_compute_keypair.keypair.public_key
}

output "private_key" {
  value = vkcs_compute_keypair.keypair.private_key
  sensitive = true
}

resource "vkcs_compute_keypair" "keypair2" {
  name = "keypair"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDhtTIlscqliqN8IxfDEngHo2qnYV8jSkKJs7YMWs8kicqTzJJK96y56B6F0/6lKFRebpL0O8A/+DhYZGw9MkEiCJtg6GpEMPpPO54AwRgHI0Xb4uaRrXTeQ4jzfZA/SCA9Ih0fMKKDN6V2PwScbrTpY13kDmBLmV3af+g87yXB2LtzGXLHLtdwc5pFGKyVykVIAWE1Q83RTDSIZFSGOn2+Rak/V7ftiVbacHyvVVpyw6sgHe8mB1kcTRK++xAXeuGn34j8q0EYJIZeNC4nHzxKepmTdjpGsf3uwEQRcmY1ZMKvMXwm0WiW5bdUO9WnMgpzXKpOlnVHtKNtdZ4uHaPH mifi@lesson"
}
