# Создаем лоадбалансер для серверов nginx
resource "vkcs_lb_loadbalancer" "lb_1" {
    name="loadballancer"
    vip_subnet_id = vkcs_networking_subnet.compute.id
}
#Создаем прослушиватель на порту 8080
resource "vkcs_lb_listener" "listener" {
  name = "listener"
  protocol = "HTTP"
  protocol_port = 8080
  loadbalancer_id = "${vkcs_lb_loadbalancer.lb_1.id}"
}
#Создаем пул балансировки с правилом рауд робин
resource "vkcs_lb_pool" "pool" {
  name = "pool"
  protocol = "HTTP"
  lb_method = "ROUND_ROBIN"
  listener_id = "${vkcs_lb_listener.listener.id}"
}
# Добавляем участников в пул балансировки
resource "vkcs_lb_members" "members_1" {
    pool_id = "${vkcs_lb_pool.pool.id}"
    count = 2
    member {
        address       = "${vkcs_compute_instance.nginx[0].access_ip_v4}"
        protocol_port = 80
    }
    member {
        address       = "${vkcs_compute_instance.nginx[1].access_ip_v4}"
        protocol_port = 80
    }
}
# Внешняя сеть
resource "vkcs_networking_floatingip" "fip" {
  pool = data.vkcs_networking_network.extnet.name
}
# Присваиваем внешний IPv4 лоад балансеру
resource "vkcs_networking_floatingip_associate" "balancer_fip" {
 floating_ip = vkcs_networking_floatingip.fip.address
 port_id = vkcs_lb_loadbalancer.lb_1.vip_port_id
}

output "lb_external_ip" {
  value = vkcs_networking_floatingip.fip.address
}

