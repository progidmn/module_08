# Шаблон конфигурации
data "vkcs_compute_flavor" "compute" {
  name = "Basic-1-2-20"
}
#Образ конфигурации
data "vkcs_images_image" "compute" {
  name = "Ubuntu-18.04-Standard"
}
# Группа серверов
resource "vkcs_compute_servergroup" "test-sg" {
  name     = "my-sg"
  policies = ["anti-affinity"]
}
# группа безопасности(firewall)
resource "vkcs_networking_secgroup" "admin" {
  name        = "secgroup_1"
  description = "My security group"
}
# правила файрвола
resource "vkcs_networking_secgroup_rule" "secgroup_rule_1" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${vkcs_networking_secgroup.admin.id}"
}
resource "vkcs_networking_secgroup_rule" "ssh" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${vkcs_networking_secgroup.admin.id}"
}

# Параметры ВМ
resource "vkcs_compute_instance" "nginx" {
  count                   = 2
  name                    = "nginx"
  flavor_id               = data.vkcs_compute_flavor.compute.id
#  security_groups         = ["default"]
  availability_zone       = "GZ1"
  user_data       = file("user_data.sh")
  key_pair = "keypair"
  block_device {
    uuid                  = data.vkcs_images_image.compute.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 8
    boot_index            = 0
    delete_on_termination = true
  }

  block_device {
    source_type           = "blank"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 8
    boot_index            = 1
    delete_on_termination = true
  }

  network {
    uuid = vkcs_networking_network.compute.id
  }
  security_groups = [
    vkcs_networking_secgroup.admin.id
  ]
  depends_on = [
    vkcs_networking_network.compute,
    vkcs_networking_subnet.compute
  ]
}

resource "vkcs_compute_instance" "react" {
  name                    = "react"
  flavor_id               = data.vkcs_compute_flavor.compute.id
#  security_groups         = ["default"]
  availability_zone       = "GZ1"
  user_data       = file("python.sh")
  key_pair = "keypair"
  block_device {
    uuid                  = data.vkcs_images_image.compute.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 8
    boot_index            = 0
    delete_on_termination = true
  }

  block_device {
    source_type           = "blank"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 8
    boot_index            = 1
    delete_on_termination = true
  }

  network {
    uuid = vkcs_networking_network.compute.id
  }
  security_groups = [
    vkcs_networking_secgroup.admin.id
  ]
  depends_on = [
    vkcs_networking_network.compute,
    vkcs_networking_subnet.compute
  ]
}

# Внешняя сеть
resource "vkcs_networking_floatingip" "react_ip" {
  pool = data.vkcs_networking_network.extnet.name
}
resource "vkcs_networking_floatingip" "nginx" {
  count = 2
  pool = data.vkcs_networking_network.extnet.name
}
# Присваиваем внешний IPv4 лоад балансеру
resource "vkcs_compute_floatingip_associate" "react_ex_ip" {
 floating_ip = vkcs_networking_floatingip.react_ip.address
 instance_id = vkcs_compute_instance.react.id
}

resource "vkcs_compute_floatingip_associate" "nginx_ex_ip" {
 count = 2
 floating_ip = vkcs_networking_floatingip.nginx[count.index].address
 instance_id = vkcs_compute_instance.nginx[count.index].id
}


output "react_external_ip" {
  value = vkcs_networking_floatingip.react_ip.address
}

